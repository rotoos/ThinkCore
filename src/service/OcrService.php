<?php

declare (strict_types=1);

namespace think\admin\service;

use TencentCloud\Common\Credential;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Ocr\V20181119\Models\BizLicenseOCRRequest;
use TencentCloud\Ocr\V20181119\Models\IDCardOCRRequest;
use TencentCloud\Ocr\V20181119\Models\VerifyEnterpriseFourFactorsRequest;
use TencentCloud\Ocr\V20181119\OcrClient;
use think\admin\Service;

/**
 * Class OcrService
 * @package think\admin\service
 * ocr识别
 * 接口请求域名： ocr.tencentcloudapi.com
 * 接口文档：https://cloud.tencent.com/document/product/866/33515
 */

class OcrService extends Service
{
    /**
     * 腾讯云的secretId
     * @var string
     */
    public $secretId;


    /**
     * 腾讯云的secretKey
     * @var string
     */
    
    protected $secretKey;


    /**
     * 控制器初始化
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function initialize()
    {
        $this->secretId = syconfig('TencentCloud','secretId');
        $this->secretKey = syconfig('TencentCloud','secretKey');
    }

    /**
     * 识别营业执照信息
     * 接口文档 https://cloud.tencent.com/document/api/866/36215
     * @param string $image
     * @return false|string|void
     * ******************************************************************
     * 参数名称	  类型	  描述
        RegNum	String	统一社会信用代码（三合一之前为注册号）
        Name	String	公司名称
        Capital	String	注册资本
        Person	String	法定代表人
        Address	String	地址
        Business	String	经营范围
        Type	String	主体类型
        Period	String	营业期限
        ComposingForm	String	组成形式
        SetDate	String	成立日期
        RecognizeWarnCode	Array of Integer	Code 告警码列表和释义：
                                                                    -20001 非营业执照
                                                                    -9102 黑白复印件告警
                                                                    注：告警码可以同时存在多个
        RecognizeWarnMsg	Array of String	告警码说明：
                                                                    OCR_WARNING_TYPE_NOT_MATCH 非营业执照
                                                                    WARN_COPY_CARD 黑白复印件告警
                                                                    注：告警信息可以同时存在多个
        RequestId	String	唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
     * *******************************************************************
     */
    public function getBizLicenseData(string $image)
    {
        try {
            // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
            // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
            // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
            $cred = new Credential($this->secretId, $this->secretKey);
            // 实例化一个http选项，可选的，没有特殊需求可以跳过
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("ocr.tencentcloudapi.com");
            // 实例化一个client选项，可选的，没有特殊需求可以跳过
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            // 实例化要请求产品的client对象,clientProfile是可选的
            $client = new OcrClient($cred, "ap-shanghai", $clientProfile);
            // 实例化一个请求对象,每个接口都会对应一个request对象
            $req = new BizLicenseOCRRequest();
            $params = array(
                'ImageUrl' => $image
            );
            $req->fromJsonString(json_encode($params));
            // 返回的resp是一个QrcodeOCRResponse的实例，与请求对象对应
            $resp = $client->BizLicenseOCR($req);
            // 输出json格式的字符串回包
            return json_decode($resp->toJsonString(),true);
        } catch (TencentCloudSDKException $e) {
            echo $e;
        }
    }

    /**
     * 识别身份证信息
     * 接口文档 https://cloud.tencent.com/document/api/866/33524
     * @param string $image
     * @param string $direction "FRONT" 正面 "BACK" 反面
     * @return mixed|void
     * ****************************************************************
     * 参数名称	类型	描述
        Name	String	姓名（人像面）
        Sex	String	性别（人像面）
        Nation	String	民族（人像面）
        Birth	String	出生日期（人像面）
        Address	String	地址（人像面）
        IdNum	String	身份证号（人像面）
        Authority	String	发证机关（国徽面）
        ValidDate	String	证件有效期（国徽面）
        AdvancedInfo	String	扩展信息，不请求则不返回，具体输入参考示例3和示例4。
        IdCard，裁剪后身份证照片的base64编码，请求 Config.CropIdCard 时返回；
        Portrait，身份证头像照片的base64编码，请求 Config.CropPortrait 时返回；

        Quality，图片质量分数，请求 Config.Quality 时返回（取值范围：0 ~ 100，分数越低越模糊，建议阈值≥50）;
        BorderCodeValue，身份证边框不完整告警阈值分数，请求 Config.BorderCheckWarn时返回（取值范围：0 ~ 100，分数越低边框遮挡可能性越低，建议阈值≤50）;

        WarnInfos，告警信息，Code 告警码列表和释义：
                                            -9100 身份证有效日期不合法告警，
                                            -9101 身份证边框不完整告警，
                                            -9102 身份证复印件告警，
                                            -9103 身份证翻拍告警，
                                            -9105 身份证框内遮挡告警，
                                            -9104 临时身份证告警，
                                            -9106 身份证 PS 告警，
                                            -9107 身份证反光告警。
        RequestId	String	唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
     * **************************************************************************
     */
    public function getIdCardData(string $image, string $direction = 'FRONT')
    {
        try {
            $cred = new Credential($this->secretId, $this->secretKey);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("ocr.tencentcloudapi.com");
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new OcrClient($cred, "ap-shanghai", $clientProfile);
            $req = new IDCardOCRRequest();
            $params = array(
                'ImageUrl' => $image,
                "CardSide" => $direction
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->IDCardOCR($req);
            return json_decode($resp->toJsonString(),true);
        } catch (TencentCloudSDKException $e) {
            echo $e;
        }
    }

    /**
     * 企业四要素核验
     * 接口文档 https://cloud.tencent.com/document/api/866/55857
     * @param string $idCard 证件号码（公司注册证件号）
     * @param string $enterpriseName 企业全称
     * @param string $realName 姓名
     * @param string $enterpriseMark 企业标识（注册号，统一社会信用代码）
     * @return mixed|void
     * ******************************************************
     * State	Integer	核验一致性（1:一致，2:不一致，3:查询无记录）
       Detail	Detail	核验结果明细，7：企业法人/负责人，6：企业股东，5：企
                        业管理人员，-21：企业名称与企业标识不符，-22：姓名不一致，-23：证件号码不一致，-24：企业名称不一致，-25：企业标识不一致
                        注意：此字段可能返回 null，表示取不到有效值。
       RequestId	String	唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
     * ******************************************************
     */
    public function VerifyEnterpriseFourFactors(string $idCard, string $enterpriseName, string $realName, string $enterpriseMark)
    {
        try {
            $cred = new Credential($this->secretId, $this->secretKey);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("ocr.tencentcloudapi.com");
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new OcrClient($cred, "ap-shanghai", $clientProfile);
            $req = new VerifyEnterpriseFourFactorsRequest();
            $params = array(
                "IdCard" => $idCard,
                "EnterpriseName" => $enterpriseName,
                "RealName" => $realName,
                "EnterpriseMark" => $enterpriseMark
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->VerifyEnterpriseFourFactors($req);
            return json_decode($resp->toJsonString(),true);
        } catch (TencentCloudSDKException $e) {
            echo $e;
        }
    }
}