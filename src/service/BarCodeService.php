<?php

declare (strict_types=1);

namespace think\admin\service;

use think\admin\Service;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Ocr\V20181119\OcrClient;
use TencentCloud\Ocr\V20181119\Models\QueryBarCodeRequest;

/**
 * Class BarCodeService
 * @package think\admin\service
 * 条码查询扩展
 * 接口请求域名： ocr.tencentcloudapi.com
 * 接口支持条形码备案信息查询，返回条形码查询结果的相关信息，包括产品名称、产品英文名称、品牌名称、规格型号、宽度、高度、深度、关键字、产品描述、厂家名称、厂家地址、企业社会信用代码13个字段信息。
 * 产品优势：直联中国物品编码中心，查询结果更加准确、可靠。
 * 接口文档：https://cloud.tencent.com/document/api/866/45513
 */

class BarCodeService extends Service
{
    /**
     * 腾讯云的secretId
     * @var string
     */
    public $secretId;


    /**
     * 腾讯云的secretKey
     * @var string
     */
    
    protected $secretKey;


    /**
     * 控制器初始化
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function initialize()
    {
        $this->secretId = syconfig('TencentCloud','secretId');
        $this->secretKey = syconfig('TencentCloud','secretKey');
    }

    /**
     * 根据条码查询相关产品备案信息
     * @param string $barcode
     * @return false|string|void
     * ***********************************************
     * BarCode	String	条码
       ProductDataRecords	Array of ProductDataRecord	条码信息数组
     * -----------------------------------------------------------
     *    名称	        类型	        描述
        ProductName	    String	   产品名称
        EnName	        String	   产品名称(英文)
        BrandName	    String	   品牌名称
        Type	        String	   规格型号
        Width	        String	   宽度，单位毫米
        Height	        String	   高度，单位毫米
        Depth	        String	   深度，单位毫米
        KeyWord	        String	   关键字
        Description	    String	   简短描述
        ImageLink	  Array of String	图片链接
        ManufacturerName	String	厂家名称
        ManufacturerAddress	String	厂家地址
        FirmCode	    String	企业社会信用代码
        CheckResult	    String	表示数据查询状态
        checkResult 状态说明
                                1  经查，该商品条码已在中国物品编码中心注册
                                2  经查，该厂商识别代码已在中国物品编码中心注册，但编码信息未按规定通报。
                                3  经查，该厂商识别代码已于xxxxx注销，请关注产品生产日期。
                                4  经查，该企业以及条码未经条码中心注册，属于违法使用
                                -1 经查，该商品条码被冒用
                                -2 经查，该厂商识别代码已在中国物品编码中心注册，但该产品已经下市
        S001 未找到该厂商识别代码的注册信息。
        S002 该厂商识别代码已经在GS1注册，但编码信息未通报
        S003 该商品条码已在GS1通报
        S004 该商品条码已注销
        S005 数字不正确。GS1前缀（3位国家/地区代码）用于特殊用途。
        E001 完整性失败：此GTIN的长度无效。
        E002 完整性失败：校验位不正确。
        E003 完整性失败：字符串包含字母数字字符。
        E004 数字不正确。GS1前缀（3位国家/地区代码）不存在。
        E005 数字不正确。GS1前缀（3位国家/地区代码）用于特殊用途。
        E006 数字不正确。尚未分配该GS1公司前缀。
        E008 经查，该企业厂商识别代码以及条码尚未通报
        CategoryCode	String	UNSPSC分类码
     * --------------------------------------------------------------
       RequestId	String	唯一请求 ID，每次请求都会返回。定位问题时需要提供该次请求的 RequestId。
     * ***********************************************
     */
    public function getBarcodeData(string $barcode)
    {
        try {
            $cred = new Credential($this->secretId, $this->secretKey);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("ocr.tencentcloudapi.com");
            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new OcrClient($cred, "ap-shanghai", $clientProfile);
            $req = new QueryBarCodeRequest();
            $params = array(
                'BarCode' => $barcode
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->QueryBarCode($req);
            return json_decode($resp->toJsonString(),true);
        } catch (TencentCloudSDKException $e) {
            echo $e;
        }
    }
}