<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 租户菜单数据模型
 * Class SysTenantMenu
 * @package think\admin\model
 */
class SysTenantMenu extends Model
{
}