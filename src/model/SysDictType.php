<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 字典类型数据模型
 * Class SysDictType
 * @package think\admin\model
 */
class SysDictType extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];
}