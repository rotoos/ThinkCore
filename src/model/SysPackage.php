<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 租户套餐数据模型
 * Class SysPackage
 * @package think\admin\model
 */
class SysPackage extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];
}