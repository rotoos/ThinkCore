<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 行业分类数据模型
 * Class SysIndustry
 * @package think\admin\model
 */
class SysIndustry extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];
}