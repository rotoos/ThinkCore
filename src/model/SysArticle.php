<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;
use think\model\relation\HasOne;

/**
 * 文章数据模型
 * Class SysArticle
 * @package think\admin\model
 */
class SysArticle extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];

    /**
     * @return HasOne
     */
    public function cate(): HasOne
    {
        return $this->hasOne(SysArticleCategory::class,'id','cate_id')->where([
            'status' => 0
        ]);
    }

    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}
