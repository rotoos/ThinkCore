<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 租户APP数据模型
 * Class SysTenantApp
 * @package think\admin\model
 */
class SysTenantApp extends Model
{
}