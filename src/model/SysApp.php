<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 应用数据模型
 * Class SysApp
 * @package think\admin\model
 */
class SysApp extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];
}