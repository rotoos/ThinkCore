<?php

declare (strict_types=1);
namespace think\admin\model;

use think\admin\Model;

/**
 * 页面数据模型
 * Class SysPage
 * @package app\admin\model
 */
class SysPage extends Model
{
    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}