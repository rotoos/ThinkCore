<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文件分组数据模型
 * Class SysUploadGroup
 * @package think\admin\model
 */
class SysUploadGroup extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];
}