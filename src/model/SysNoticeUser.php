<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 通知已读数据模型
 * Class SysNoticeUser
 * @package think\admin\model
 */
class SysNoticeUser extends Model
{
    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}