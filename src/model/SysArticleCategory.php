<?php

declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文章分类数据模型
 * Class SysArticleCategory
 * @package think\admin\model
 */
class SysArticleCategory extends Model
{
    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at','updated_by','created_at','created_by'
        
    ];

    /**
     * 格式化创建时间
     * @param string $value
     * @return string
     */
    public function getCreateAtAttr(string $value): string
    {
        return format_datetime($value);
    }
}

